import express, {Request, Response } from 'express';
import path from 'path';
//import dotenv from 'dotenv';
import apiroutes from './routes/api'
import cors from 'cors';



//dotenv.config();

const app = express();

app.use(cors({
    origin: '*'
}));

app.use(express.static(path.join(__dirname, '../public')));
app.use(express.urlencoded({extended: true}));

app.use( express.json() );



app.use('/api',apiroutes)


app.use((req:Request, res:Response)=>{
    let url = req.protocol + req.originalUrl;
    console.log('Endpoint nao encontrado Deploy Jankins');
    console.log(req.body);
    console.log(url);
    

    res.status(404);
    res.json({
        error: 'Endpoint não encontrado (teste de webhook)',
        url: url
    })
})

console.log('Iniciando aplicação na porta NOVO CODIGO CodeDeploy '+ 9010);
app.listen(9010);

